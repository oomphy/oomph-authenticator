var fakeOomph = '(' + function() {
  var initReceiptThing = function() {
    var logger = document.getElementById('oomph-auth-debug');
    if (!logger) {
      var elem = document.createElement('div');
      elem['id'] = 'oomph-auth-debug';
      document.body.appendChild(elem);
      logger = elem;
    }
    return logger;
  }
  window.Oomph = {}
  window.Oomph.register_unlock = function(receipt) {
    var logger = initReceiptThing();
    console.log('Unlock receipt: ' + receipt);
  };
  window.Oomph.register_subscription = function(receipt, start_date, end_date) {
    var logger = initReceiptThing();
    console.log('Subscription receipt: ' + receipt + ' from:' + start_date + ' to:' + end_date);
  };
} + ')();';

var script = document.createElement('script');
script.textContent = fakeOomph;
(document.head||document.documentElement).appendChild(script);
script.parentNode.removeChild(script);

function doBeforeLoad(event){
    if (event.srcElement.tagName=="SCRIPT" && /oomph-authenticator-0.1.js/.test(event.srcElement.src)) {
        console.log('Replacing on-device `Oomph` api with tester');
        event.preventDefault();
    }
}
document.addEventListener('beforeload', doBeforeLoad , true);
