
Authenticator Debugger, Chrome Extension
================

Install
------

* Navigate to "chrome://extensions"
* Tick 'Developer Mode'
* Click "Load unpacked extension..."
* Navigate to this folder, and select it.
* Turn on 'Allow in incognito'. Useful for testing.

All receipt saving will now be visible in the javascript console in Chrome.
