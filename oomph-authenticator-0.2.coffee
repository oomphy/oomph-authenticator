class OomphURI
  @unlock: (receipt) -> "oomph://authenticated/unlock?receipt=#{escape(receipt)}"
  @subscription: (receipt, start_date, end_date) -> 
    "oomph://authenticated/subscription?receipt=#{escape(receipt)}&start_date=#{start_date}&end_date=#{end_date}"
  @guestmode: () -> "oomph://authenticated/guestmode"
  @dismissAuthenticator: (delay) -> 
    delay ?= 0
    "oomph://authenticated/dismiss_authenticator?delay=#{delay}"

class @Oomph
  # this is type detection, which we use in the register_subscription method
  # to determine whether the arguments are actual Date objects or not
  @getType: (obj) ->
    if obj == undefined or obj == null
      return String obj
    classToType = new Object
    for name in "Boolean Number String Function Array Date RegExp".split(" ")
      classToType["[object " + name + "]"] = name.toLowerCase()
    myClass = Object.prototype.toString.call obj
    if myClass of classToType
      return classToType[myClass]
    return "object"
	
  @register_unlock: (receipt) ->
    this.call_uri(OomphURI.unlock(receipt))

  @register_subscription: (receipt, start_date, end_date) ->
    if this.getType(start_date) == "date"
      start_date = start_date.getTime()
    if this.getType(end_date) == "date"
      end_date = end_date.getTime()
    start_date *= 1000 if start_date < 1000000000000
    end_date *= 1000 if end_date < 1000000000000
    this.call_uri(OomphURI.subscription(receipt, start_date, end_date))
	
  @register_guestmode: () ->
    this.call_uri(OomphURI.guestmode())

  @dismiss_authenticator_dialog: (delay) ->
    this.call_uri(OomphURI.dismissAuthenticator(delay))

  # This is how data is passed to the UIWebview delegate in iOS. iFrames are used because
  # XMLHttpRequest calls are not intercepted by the delegate.
  @call_uri: (uri) ->
    frame = document.createElement("IFRAME")
    frame.setAttribute("src", uri)
    document.body.appendChild(frame)
    frame.parentNode.removeChild(frame)
