Oomph Authenticators
====================

Authenticators are services that can be used to add authentication to Oomph apps, or to provide subscription-like capabilities outside of the App Store.

An authenticator for an app must provide 2 things:

    * An authentication mobile site.
    * A token verification API.

### Authentication Mobile Site

This is a user facing site, which will be presented to the user whenever authorisation is needed.

When the user authenticates successfully, the page should call the Oomph javascript API.

The following calls are available in all versions of the API:

    // If the user has full access to the application
    Oomph.register_unlock(<encoded_receipt_string>); 

	// Time limited access.
    // If the user has subscription details, then this call will record them in the app.
    // start_date and end_date should be either JavaScript Date objects or
	// numeric UNIX timestamps in seconds or milliseconds
    Oomph.register_subscription(<encoded_receipt_string>, start_date, end_date);

The following calls were added in Version 0.2, available in Oomph 20 and later:

	// This call will allow guest access to the app.
	// With guest access enabled, the app displays public issues only.
    Oomph.register_guestmode(); 

	// This call will dismiss the authenticator dialog (the equivalent of tapping the "Done" button).
	// You can pass in an optional delay time before the authenticator dialog is dismissed.
	// If the delay argument is not present then the authenticator dialog will be dismissed as soon as
	// this method is called.
	Oomph.dismiss_authenticator_dialog(<delay in seconds>);

The encoded receipts will be passed to the verification API when a user attempts to download an issue.

### Verification API

When requested by the app, our server will send a POST request to the verification URL that is configured for your app. 

In your implementation of the verification API you should examine the receipt passed as a parameter and decide whether or not it is valid before constructing and returning an appropriate JSON response.

        Method: POST
        Params: receipt - the receipt string as provided to the javascript API
                udid - the UDID of the device requesting access

        Return format: JSON
        Required Keys:

                 status: [ok|invalid]      (String type)

        Optional Keys for subscription receipts:

                 [start_date]: timestamp   (Numeric type)
                 [end_date]:   timestamp   (Numeric type)

		This is an example of a valid response:
		
		         { "status":"ok", "start_date":1353888000000, "end_date":1356912000000 }

