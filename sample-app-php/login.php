<?php

// grab the username and password from the POST
$username 	= $_POST['username'];
$password 	= $_POST['password'];

// set defaults
$loggedIn 	= false;
$startDate 	= 0;
$endDate 	= 0;
$receipt 	= "";

// check to see if the credentials are valid. In a real implementation
// you would access your user database and check that the user is valid
if($username == 'demo' && $password == 'test')
{
	// set the logged in status
	$loggedIn = true;
	
	// set the subscription start and end dates, normally this would come
	// from your database
	$startDate = time() * 1000;
	$endDate = (time() + 3600) * 1000;
	
	// construct a receipt. For this example we're just hashing the username
	// and password, but in a real implementation you would probably use
	// an encoded version of the user's unique ID
	$receipt = md5($username.$password);
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Login</title>
		<link href="stylesheets/bootstrap-1.3.0.min.css" media="screen" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<?php
			if(!$loggedIn)
			{
		?>
		<h3>Sorry, but your login details are incorrect. Please try again.</h3>
		<form action="login.php" method="post" accept-charset="utf-8">
			<p><label for="username">Username</label><input id="username" type="text" name="username" value="" /></p>
			<p><label for="password">Password</label><input id="password" type="text" name="password" value="" /></p>
			<p><input type="submit" value="Submit" /></p>
		</form>
		<?php
			}
			else
			{
		?>
		<h3>Congratulations, you have been granted access.</h3>
		<script>
		<?php 
		//output JavaScript code to call the Oomph API when the page loads in the app's msite web view
		echo ('Oomph.register_subscription("'.$receipt.'", '.$startDate.', '.$endDate.');');
		?>
		</script>
		<?php		
			}
		?>
	</body>
</html>
