<?php 
// set the Content-Type HTTP header to the JSON MIME type
header('Content-Type: application/json'); 

// define a simple function to build and output the JSON response
function JSONResponse($status, $startDate = null, $endDate = null)
{
	if(!$startDate && !$endDate)
		echo ("{ 'status' : '$status' }");
	else
		echo ("{ 'status' : '$status', 'start_date' : $startDate, 'end_date' : $endDate }");
}

// grab the POST parameters
$receipt = isset($_POST["receipt"]) ? $_POST["receipt"] : "";
$udid    = isset($_POST["udid"]) ? $_POST["udid"] : "";  //we don't actually use this

// validate the receipt, in a real implementation you would probably access a user
// database and find a matching user
if($receipt == md5('demotest'))
{
	// construct and output a time-based subscription JSON response
	// this will return a payload similar to the following:
	// { "status": "ok", "start_date":1353888000000, "end_date":1356912000000 }
	JSONResponse("ok", time() * 1000, (time() + 3600) * 1000);
	
	// for a non-time-based unlock, you'd simply use the following:
	// JSONResponse("ok");
}
else
{
	// construct and output a JSON response that rejects the user
	JSONResponse("invalid");
}
?>
