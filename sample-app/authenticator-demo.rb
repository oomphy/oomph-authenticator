require 'sinatra/base'
require 'sinatra/flash'
require 'base64'
require 'json'
require 'haml'

class AuthenticatorDemo < Sinatra::Base
  enable :sessions
  register Sinatra::Flash

  get('/') { haml :index }

  post '/login' do
    token_value = params[:token]
    if token_value == "demo"
      @encoded = Base64.encode64({:token => token_value}.to_json).strip()
      haml :login
    else
      flash[:error] = "Invalid token."
      redirect '/'
    end
  end

  post '/guestmode' do
    haml :guestmode
  end
  
  post '/dismiss' do
      haml :dismiss
  end  

  post '/api/verify' do
    content_type :json
     receipt = JSON.parse(Base64.decode64(params[:receipt]))
     status = if receipt["token"] == "demo" then 
                "ok" 
              else 
                "invalid" 
              end
     {
       "status" => status
     }.to_json
  end

  if $0 == __FILE__ then
    run!
  end
end

